/*
 * https://fgiesen.wordpress.com/2011/11/21/buffer-centric-io/
 */

typedef int	BufioError;
enum {
	BUFIO_NOERROR =	 0,
	BUFIO_EOF =	-1
};

typedef struct bufio_stream	BufioStream;
struct bufio_stream {
	const uchar	*start;
	const uchar	*end;
	const uchar	*cur;
	BufioError	(*refill)(BufioStream *);
	BufioError	 err;
};

void		bufio_initnullstream(BufioStream *);
void		bufio_initmemstream(BufioStream *, const uchar *, size_t);
BufioError	bufio_fail(BufioStream *, BufioError);

int	bufio_checklen(BufioStream *, size_t);
int	bufio_copy(void *, BufioStream *, size_t);

ushort	bufio_bshort(BufioStream *);
uint	bufio_bint(BufioStream *);
ushort	bufio_lshort(BufioStream *);
uint	bufio_lint(BufioStream *);
