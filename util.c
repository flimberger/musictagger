#include "tagger.h"

#include <stdarg.h>
#include <stdlib.h>

extern const char	*__progname;

void
err(const char *msg, ...)
{
	va_list  ap;
	int	 savederr;

	savederr = errno;
	va_start(ap, msg);
	fprintf(stderr, "%s: ", __progname);
	vfprintf(stderr, msg, ap);
	fprintf(stderr, ": %s\n", strerror(savederr));
	va_end(ap);
	exit(1);
}

void
errc(int ec, const char *msg, ...)
{
	va_list	ap;

	va_start(ap, msg);
	fprintf(stderr, "%s: %s: ", __progname, strerror(ec));
	vfprintf(stderr, msg, ap);
	exit(1);
}

void
errx(const char *msg, ...)
{
	va_list	 ap;

	va_start(ap, msg);
	fprintf(stderr, "%s: ", __progname);
	vfprintf(stderr, msg, ap);
	va_end(ap);
	exit(1);
}

void
printv(const char *msg, ...)
{
	va_list	ap;

	if (verbose) {
		va_start(ap, msg);
		vfprintf(stdout, msg, ap);
		va_end(ap);
	}
}
