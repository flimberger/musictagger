#include "tagger.h"

/* int is assumed to have at least 4 bytes */
struct taghdr {
	int	len;
	uchar	flags;
};
#define TUNSYN	0x80
#define HASEXT	0x40
#define ISEXPE	0x20
#define HASFOT	0x10
#define HDRPAD	0x0F
#define THSIZE	10

struct exthdr {
	int	len;
	uchar	flags;
};
#define ISUPDT	0x40
#define HASCRC	0x20
#define ISREST	0x10
#define EXTPAD	0x8F

struct frmhdr {
	int	 len;
	char	 id[4];
	ushort	 flags;
};
#define TAGALT	0x4000
#define FRMALT	0x2000
#define RDONLY	0x1000
#define FGRPID	0x0040
#define COMPR	0x0008
#define ENCRPT	0x0004
#define FUNSYN	0x0002
#define DATLEN	0x0001
#define FRMPAD	0x1F1F
#define FHSIZE	10

enum txtenc {
	ISO85591	= 0x00,
	UTF16BOM	= 0x01,
	UTF16BE		= 0x02,
	UTF8		= 0x03
};

static int	getssi(BufioStream *);
static void	readfh(struct frmhdr *, BufioStream *);

int
readid3v2tags(BufioStream *s)
{
	struct taghdr	 th;
	struct exthdr	 eh;
	struct frmhdr	 fh;
	int		 ftotal, vsn;
	uchar		 b;

	if (!bufio_checklen(s, THSIZE))
		errx("not enough data for tag header");
	if (strncmp((char*)s->cur, "ID3", 3))
		return 0;	/* untagged */
	s->cur += 3;
	vsn = *s->cur++;
	if (vsn != 3 && vsn != 4)
		err("unsupported ID3v2.%d", vsn);
	if (*s->cur != 0)
		errx("expected null byte, got %d instead", *s->cur);
	s->cur++;
	th.flags = *s->cur++;
	if (th.flags & HDRPAD)
		errx("invalid padding bits set on taghdr flag byte");
	th.len = getssi(s);
	if (verbose) {
		printf("ID3v2.%d, %d bytes", vsn, th.len);
		if (th.flags & TUNSYN)
			printf(" unsynchronization");
		if (th.flags & ISEXPE)
			printf(" experimental");
		if (th.flags & HASFOT)
			printf(" with footer");
		printf("\n");
	}

	eh.len = -1;
	if (th.flags & HASEXT) {
		eh.len = getssi(s);
		if (verbose) {
			printf("extended header length: %d bytes", eh.len);
			if (!bufio_checklen(s, eh.len - 4))
				errx(
				    "not enough data for extended tag header");
			eh.flags = *s->cur++;
			if (eh.flags & EXTPAD)
				errx(
				    "invalid padding bits set on exthdr flag byte");
			if (eh.flags & ISUPDT)
				puts(" update tag");
			if (eh.flags & HASCRC)
				puts(" CRC-32");
			if (eh.flags & ISREST)
				printf(" restrictions");
			puts("\n");
		}
	}

	ftotal = 0;
	while (ftotal < th.len) {
		if (*s->cur == 0) {	/* padding */
			if (th.flags & HASFOT)
				errx("null byte encountered, expected footer");
			s->cur += th.len - ftotal;
			break;
		}
		readfh(&fh, s);
		printf("%c%c%c%c: ", fh.id[0], fh.id[1], fh.id[2], fh.id[3]);
		if (verbose)
			printf("%d byte%s", fh.len, fh.len > 1 ? "s" : "");
		if (verbose) {
			if (fh.flags & FUNSYN)
				printf(" unsynchronized");
			if (fh.flags & FGRPID)
				printf(" grouped");
			if (fh.flags & COMPR)
				printf(" compressed");
			if (fh.flags & ENCRPT)
				printf(" encrypted");
			if (fh.flags & FUNSYN)
				printf(" unsynchronized");
			if (fh.flags & DATLEN)
				printf(" with data length indicator");
		}
		printf(": ");
		if (!bufio_checklen(s, fh.len))
			errx("not enough data for frame data");
		if (fh.id[0] == 'T') {
			b = *s->cur++;
			if (b != ISO85591 && b != UTF8)
				printf("unsupported encoding");
			else {
				fwrite(s->cur, fh.len - 1, 1, stdout);
			}
			s->cur += fh.len - 1;
		} else if (!strncmp(fh.id, "COMM", 4)) {
			printf("[%c%c%c] %s", *(s->cur + 1), *(s->cur + 2),
			    *(s->cur + 3), s->cur + 4);
			s->cur += fh.len;
		} else {
			printf("unsupported tag data");
			s->cur += fh.len;
		}
		printf("\n");
		ftotal += fh.len;
	}
	return 0;
}

static int
getssi(BufioStream *s)
{
	int i, res;

	if (!bufio_checklen(s, 4))
		errx("sync safe integer truncated");
	for (i = res = 0; i < 4; i++)
		res |= (*s->cur++ & 0x7F) << 7 * (4 - i - 1);

	return res;
}

static void
readfh(struct frmhdr *fh, BufioStream *s)
{
	uchar	b;
	int	i;

	if (!bufio_checklen(s, 10))
		errx("frame header truncated");

	for (i = 0; i < 4; i++) {
		b = *s->cur++;
		if (((b < 'A') || (b > 'Z')) && ((b < '0') || (b > '9')))
			errx("invalid frame ID byte %d", b);
		fh->id[i] = b;
	}
	fh->len = getssi(s);
	fh->flags = *s->cur++ << 8;
	fh->flags |= *s->cur++;
	if (fh->flags & FRMPAD)
		errx("invalid padding bits set on frmhdr flag halfword");
}
