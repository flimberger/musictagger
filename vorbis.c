#include "tagger.h"

#define OGGPGHDRSZ	27

enum metadatatype {
	STREAMINFO = 0,
	PADDING,
	APPLICATION,
	SEEKTABLE,
	VORBIS_COMMENT,
	CUESHEET,
	PICTURE,
	RESERVED
};

enum oggpacketid {
	ID =		1,
	COMMENT =	3,
	SETUP =		5
};

typedef struct {
	BufioStream	 s;
	BufioStream	*src;
} PageStream;

static uchar	buf[4096];
static const char	*blocknames[] = {
	"STREAMINFO",
	"PADDING",
	"APPLICATION",
	"SEEKTABLE",
	"VORBIS_COMMENT",
	"CUESHEET",
	"PICTURE"
};

static BufioError	refill_page(BufioStream *);
static BufioError	init_pagestream(PageStream *, BufioStream *);
static int		readvorbispacket(BufioStream *);
static int		readvorbiscomments(BufioStream *);

int
readflac(BufioStream *s)
{
	int	lastblock, blocktype;
	uint	blocksize;
	uchar	hflags;

	if (!bufio_checklen(s, 4))
		errx("not enough data for flac magic number");
	if (strncmp((const char *)s->cur, "fLaC", 4))
		errx("not a flac file");
	s->cur += 4;
	lastblock = 0;
	while (!lastblock) {
		if (!bufio_checklen(s, 4))
			errx("not enough data for metadata header");
		hflags = *s->cur++;
		lastblock = hflags & 0x80;
		blocktype = hflags & 0x7F;
		if (blocktype == 127)
			errx("invalid metadata block type");
		else if (blocktype >= RESERVED)
			printf("block type: reserved");
		else
			printf("block type: %s", blocknames[blocktype]);
		blocksize = bufio_bshort(s) << 8;
		blocksize |= *s->cur++;
		printf(": %d bytes\n", blocksize);
		if (!bufio_checklen(s, blocksize))
			errx("not enough data for metadata block");
		if (blocktype == VORBIS_COMMENT)
			return readvorbiscomments(s);
		else
			s->cur += blocksize;
	}
	return -1;
}

int
readogg(BufioStream *s)
{
	PageStream	ps;
	int	processing;

	if (init_pagestream(&ps, s) != BUFIO_NOERROR)
		errx("failed to initialize OGG container stream");
	processing = 1;
	while (processing) {
		processing = readvorbispacket(&ps.s);
	}
	return -1;
}

static BufioError
refill_page(BufioStream *s)
{
	BufioStream	*src;
	PageStream	*ps;
	int		 i, eos, nseg, psz;
	uint		 psn, ssn;
	uchar		 b;

	printf("refill page\n");
	ps = (PageStream *)s;
	src = ps->src;

	if (!bufio_checklen(src, OGGPGHDRSZ))
		errx("not enough data for ogg container page header");
	/* TODO: might be "fLaC" or other if not the first one */
	if (strncmp((const char *)src->cur, "OggS", 4))
		errx("not an ogg page (offset 0x%08lx)", s->cur - s->start);
	src->cur += 4;
	b = *src->cur++;
	if (b != 0x00)
		errx("unknown stream structure version 0x%02x", b);
	b = *src->cur++;
	eos = b & 0x04;
	/* this is the absolute granule position, which we don't need */
	src->cur += 8;
	ssn = bufio_lint(src);
	printf("stream serial number: %u\n", ssn);
	psn = bufio_lint(src);
	printf("page sequence number: %u\n", psn);
	/* checksum, skipped */
	src->cur += 4;
	nseg = *src->cur++;
	printf("%d segments\n", nseg);
	psz = 0;
	for (i = 0; i < nseg; i++) {
		b = *src->cur++;
		printf("segment size: %d\n", b);
		psz += b;
	}
	printf("page data size: %d\n", psz);
	if (!bufio_checklen(src, psz))
		errx("not enough data for ogg container packet");
	s->start = src->cur;
	s->cur = src->cur;
	src->cur += psz;
	s->end = src->cur;
	if (eos)
		return bufio_fail(s, BUFIO_EOF);
	return BUFIO_NOERROR;
}

static BufioError
init_pagestream(PageStream *s, BufioStream *src)
{
	s->src = src;
	s->s.err = BUFIO_NOERROR;
	s->s.refill = refill_page;
	return s->s.refill(&s->s);
}

static int
readvorbispacket(BufioStream *s)
{
	ptrdiff_t	remaining;
	uint		version;
	int		res;
	uchar		packtype;

	res = 1;
	if (!bufio_checklen(s, 7))
		errx("not enough data for package header");
	packtype = *s->cur++;
	if (strncmp((const char *)s->cur, "vorbis", 6))
		errx("invalid package header");
	s->cur += 6;
	if (packtype == ID) {
		printf("identification header\n");
		if (!bufio_checklen(s, 23))
			errx("not enough data for identification header");
		version = bufio_lint(s);
		printf("vorbis version %u\n", version);
		s->cur += 19;
	} else if (packtype == COMMENT) {
		printf("comment header\n");
		/* res = */ readvorbiscomments(s);
		/* read final framing bit */
		if (*s->cur++ != 1)
			errx("framing bit not set");
	} else if (packtype == SETUP) {
		printf("setup header\n");
	} else {
		printf("unknown package type %d\n", packtype);
	}
	remaining = s->end - s->cur;
	if (remaining == 0)
		if (s->refill(s) != BUFIO_NOERROR)
			errx("refill failed");
	return res;
}

static int
readvorbiscomments(BufioStream *s)
{
	uint	i, ilen, vlen;

	ilen = bufio_lint(s);
	if (bufio_copy(buf, s, ilen) < 0)
		errx("not enough data for vendor name");
	buf[ilen] = '\0';
	printf("vendor string: %s\n", buf);
	vlen = bufio_lint(s);
	for (i = 0; i < vlen; i++) {
		ilen = bufio_lint(s);
		if (ilen > 4096) {
			printf("warning: metadata too big\n");
			s->cur += ilen;
			continue;
		}
		if (bufio_copy(buf, s, ilen) < 0)
			errx("not enough data for comment");
		buf[ilen] = '\0';
		printf("%s\n", buf);
	}
	return 0;
}
