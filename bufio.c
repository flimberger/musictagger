/*
 * https://fgiesen.wordpress.com/2011/11/21/buffer-centric-io/
 */

#include <string.h>

#include <platform.h>
#include "bufio.h"

static BufioError
refillzeros(BufioStream *s)
{
	static const uchar	zeros[128] = { 0 };

	s->start = zeros;
	s->cur = zeros;
	s->end = zeros + sizeof(zeros);
	return s->err;
}

void
bufio_initnullstream(BufioStream *s)
{
	s->refill = refillzeros;
	s->err = BUFIO_NOERROR;
	s->refill(s);
}

static BufioError
refillmem(BufioStream *s)
{
	return bufio_fail(s, BUFIO_EOF);
}

void
bufio_initmemstream(BufioStream *s, const uchar *p, size_t len)
{
	s->start = p;
	s->cur = p;
	s->end = p + len;
	s->refill = refillmem;
	s->err = BUFIO_NOERROR;
}

BufioError
bufio_fail(BufioStream *s, BufioError err)
{
	s->err = err;
	s->refill = refillzeros;
	return s->refill(s);
}

int
bufio_checklen(BufioStream *s, size_t l)
{
	return s->cur + l <= s->end;
}

int
bufio_copy(void *p, BufioStream *s, size_t l)
{
	if (!bufio_checklen(s, l))
		return -1;
	memcpy(p, s->cur, l);
	s->cur += l;
	return l;
}

ushort
bufio_bshort(BufioStream *s)
{
	ushort	res;

	res = *s->cur++ << 8;
	res |= *s->cur++;
	return res;
}

uint
bufio_bint(BufioStream *s)
{
	uint	res;

	res = bufio_bshort(s) << 16;
	res |= bufio_bshort(s);
	return res;
}

ushort
bufio_lshort(BufioStream *s)
{
	ushort	res;

	res = *s->cur++;
	res |= *s->cur++ << 8;
	return res;
}

uint
bufio_lint(BufioStream *s)
{
	uint	res;

	res = bufio_lshort(s);
	res |= bufio_lshort(s) << 16;
	return res;
}
