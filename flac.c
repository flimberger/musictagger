#include "tagger.h"

enum metadatatype {
	STREAMINFO = 0,
	PADDING,
	APPLICATION,
	SEEKTABLE,
	VORBIS_COMMENT,
	CUESHEET,
	PICTURE,
	RESERVED
};

static const char	*blocknames[] = {
	"STREAMINFO",
	"PADDING",
	"APPLICATION",
	"SEEKTABLE",
	"VORBIS_COMMENT",
	"CUESHEET",
	"PICTURE"
};

int
readflacblocks(BufioStream *s)
{
	int	lastblock, blocktype;
	uint	blocksize;
	uchar	hflags;

	if (!bufio_checklen(s, 4))
		errx(1, "not enough data for flac magic number");
	if (strncmp((const char *)s->cur, "fLaC", 4))
		errx(1, "not a flac file");
	s->cur += 4;
	lastblock = 0;
	while (!lastblock) {
		if (!bufio_checklen(s, 4))
			errx(1, "not enough data for metadata header");
		hflags = *s->cur++;
		lastblock = hflags & 0x80;
		blocktype = hflags & 0x7F;
		if (blocktype == 127)
			errx(1, "invalid metadata block type");
		else if (blocktype >= RESERVED)
			printf("block type: reserved");
		else
			printf("block type: %s", blocknames[blocktype]);
		blocksize = bufio_bshort(s) << 8;
		blocksize |= *s->cur++;
		printf(": %d bytes\n", blocksize);
		if (!bufio_checklen(s, blocksize))
			errx(1, "not enough data for metadata block");
		if (blocktype == VORBIS_COMMENT)
			return readvorbiscomments(s);
		else
			s->cur += blocksize;
	}
	return -1;
}
