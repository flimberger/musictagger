MACH=	gcc/amd64
OBJS=	tagger.o bufio.o id3v1.o id3v2.o mp3.o util.c vorbis.c
CFLAGS=	-std=c89 -Wall -Wextra -Wpedantic -g -I$(MACH)
musictagger:	$(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS)
$(OBJS):	tagger.h bufio.h
clean:
	rm -f *.core *.o musictagger
tags:	*.c *.h
	ctags -d *.c *.h
