#include <stddef.h>

typedef unsigned char	uchar;
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;

typedef ulong	Rune;

/* Ignore warnings for strict standard adherence,
 * because this is compiler-specific code already.
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wlong-long"
typedef unsigned long long	uvlong;
typedef unsigned long long	uintptr;
#pragma GCC diagnostic pop

#define memset(p, z, n)	__builtin_memset((p), (z), (n))
