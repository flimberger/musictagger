#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "tagger.h"

enum file_type {
	UNKNOWN,
	FLAC,
	MP3,
	OGG
};

extern const char	*__progname;

int	verbose;

static void		dofile(const char *);
static void		filemapstream(BufioStream *, const char *);
static void		unmapstream(BufioStream *, const char *);
static enum file_type	taste(BufioStream *);
static void		usage(void);

int
main(int argc, char *argv[])
{
	int i;

	while ((i = getopt(argc, argv, "v")) > 0)
		switch (i) {
		case 'v':
			verbose = 1;
			break;
		default:
			usage();
			break;
		}
	argc -= optind;
	argv += optind;
	if (argc == 0)
		usage();
	for (i = 0; argv[i]; i++) {
		dofile(argv[i]);
	}
}

static void
dofile(const char* path)
{
	BufioStream	s;
	enum file_type	t;
	int		r;

	filemapstream(&s, path);
	t = taste(&s);
	r = -1;
	switch (t) {
	case FLAC:
		printf("%s is a flac file\n", path);
		r = readflac(&s);
		break;
	case MP3:
		printf("%s is a mp3 file\n", path);
		r = readmp3tags(&s);
		break;
	case OGG:
		printf("%s is an ogg file\n", path);
		r = readogg(&s);
		break;
	default:
		printf("%s is an unknown file\n", path);
		break;
	}
	unmapstream(&s, path);
	if (r < 0)
		printf("no tags processed\n");
}

static void
filemapstream(BufioStream *s, const char *path)
{
	struct stat	 sb;
	uchar		*p;
	int		 fd;

	if ((fd = open(path, O_RDONLY)) < 0)
		err("failed to open file %s", path);
	if (fstat(fd, &sb) < 0)
		err("failed to stat file %s", path);
	p = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (p == MAP_FAILED)
		err("failed to map file %s", path);
	bufio_initmemstream(s, p, sb.st_size);
	close(fd);
}

static void
unmapstream(BufioStream *s, const char *path)
{
	if (munmap((void *)s->start, s->end - s->start) < 0)
		err("failed to unmap file %s", path);
	bufio_initnullstream(s);
}

static enum file_type
taste(BufioStream *s)
{
	if (!strncmp((const char *)s->cur, "fLaC", 4))
		return FLAC;
	else if (!strncmp((const char *)s->cur, "OggS", 4))
		return OGG;
	else if (!strncmp((const char *)s->cur, "ID3", 3))
		return MP3;
	else if (readmp3header(s)) {
		s->cur = s->start;
		return MP3;
	}
	return UNKNOWN;
}

static void
usage(void)
{
	fprintf(stderr, "usage: %s [-v] file\n", __progname);
	exit(2);
}
