#include "tagger.h"

int
readmp3header(BufioStream *s)
{
	int	br, fr, pd;
	uchar	b;

	if (!bufio_checklen(s, 4))
		err("failed to read mp3 header: not enough data");
	if (*s->cur++ != 0xFF)
		return 0;
	switch ((*s->cur++ & 0xFE)) {
	case 0xFA:
		printf("MPEG-1");
		break;
	case 0xF2:
		printf("MPEG-2");
		break;
	case 0xE2:
		printf("MPEG-2.5");
		break;
	default:
		return 0;
	}
	b = *s->cur++;
	br = b & 0xF0;
	fr = b & 0x0C;
	pd = b & 0x02;
	printf(" Audio Layer III, bitrate=%d, samplerate=%d", br, fr);
	if (pd)
		printf(", padded");
	printf("\n");
	return 1;
}

int
readmp3tags(BufioStream *s)
{
	int	r;

	r = -1;
	if (!strncmp((const char *)s->cur, "ID3", 3))
		r = readid3v2tags(s);
	if (!r)
		readid3v1tags(s);
	else
		r = readid3v1tags(s);
	return r;
}
