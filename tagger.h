#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <platform.h>
#include "bufio.h"

struct errctx {
	const char	*msg;
	int		 errc;
};

extern int	verbose;

/* mp3 */
int	readmp3header(BufioStream *);
int	readmp3tags(BufioStream *);
int	readid3v1tags(BufioStream *);
int	readid3v2tags(BufioStream *);

/* vorbis */
int	readflac(BufioStream *);
int	readogg(BufioStream *);

/* util */
void	err(const char *, ...);
void	errc(int ec, const char *, ...);
void	errx(const char *, ...);
void	printv(const char *, ...);
