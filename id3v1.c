#include "tagger.h"

#define TAGLEN	30

static void	readfield(BufioStream *, char *);

int
readid3v1tags(BufioStream *s)
{
	char	data[TAGLEN + 1];

	if (!bufio_checklen(s, 128))
		errx("not enough data for ID3v1 tags");
	s->cur = s->end - 128;
	if (strncmp((const char *)s->cur, "TAG", 3))
		return -1;
	s->cur += 3;
	printv("ID3v1 tag:\n");
	data[TAGLEN] = '\0';
	readfield(s, data);
	printf("title: %s\n", data);
	readfield(s, data);
	printf("artist: %s\n", data);
	readfield(s, data);
	printf("album: %s\n", data);
	bufio_copy(data, s, 4);
	printf("year: %c%c%c%c\n", data[0], data[1], data[2], data[3]);
	readfield(s, data);
	printf("comment: %s\n", data);
	if (!data[TAGLEN - 2])
		printf("track number: %d\n", data[TAGLEN - 1]);
	if (*s->cur < 255)
		printf("genre id: %d\n", *s->cur);
	s->cur++;
	return 0;
}

static void
readfield(BufioStream *s, char *buf)
{
	char	*p;

	bufio_copy(buf, s, TAGLEN);
	/* remove padding: either spaces or null bytes
	 * null bytes are fine already, so just convert all trailing spaces
	 * to null bytes
	 */
	p = buf + TAGLEN;
	while (p > buf) {
		if (*p == ' ')
			*p = '\0';
		else
			break;
	}
}
